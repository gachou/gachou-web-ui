module.exports = {
  plugins: ["react-hooks"],
  extends: ["prettier", "eslint:recommended"],
  ignorePatterns: ["build/"],
  parserOptions: {
    sourceType: "module",
    ecmaVersion: 2021,
  },
  rules: {
    eqeqeq: ["error", "always", { null: "ignore" }],
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": [
      "warn",
      {
        additionalHooks: "(useRecoilCallback|useRecoilTransaction_UNSTABLE)",
      },
    ],
  },
  overrides: [
    // typescript files
    {
      files: "**/*.+(ts|tsx)",
      parser: "@typescript-eslint/parser",
      parserOptions: {
        project: "./tsconfig.json",
      },
      plugins: ["@typescript-eslint/eslint-plugin"],
      extends: [
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended",
      ],
    },
    {
      // Configuration files using NodeJS
      files: [
        "./*.config.js",
        "./.eslintrc.js",
        "./.eslintrc.js",
        "./.storybook/main.js",
        "./scripts/**/*.js",
      ],
      parserOptions: {
        sourceType: "script",
        ecmaVersion: 2021,
      },
      env: {
        node: true,
      },
    },
    {
      // Configuration files using NodeJS + TypeScript
      files: ["./*.config.ts", "./vite-plugins/**/*.ts"],
      parser: "@typescript-eslint/parser",
      parserOptions: {
        project: "./tsconfig.node.json",
      },
      env: {
        node: true,
      },
    },
  ],
};
