# gachou-web-ui

Web-Frontend for the Gachou picture album.

## Usage

### Docker-build

There is a docker-build that is created with each pipeline. The image can be downloaded from the local
docker-registry at https://gitlab.com/gachou/gachou-web-ui/-/packages

The build contains a `Caddyfile` with non-https configuration, and the compiled files of the web-ui.
The supposed use case is to have another reverse-proxy in front of the Caddy, which can then do https.

But you can also build a new docker-image based on this one, and replace the `Caddyfile`

### Release files

TODO:

### Configuration

A configuration file "gachou-config.js" in the same folder as "index.html" is read by the application.
It modifies the global object "GACHOU_WEB_UI_CONFIG" to inject the configuration.

You can overwrite this file to provide your own configuration to the frontend.

> **Warning:** the name "gachou-config.js" has no hash-component, so you
> must make sure that the file is not cached by the browser.
> Set the caching headers accordingly.

The [config/](config/)-folder contains example configurations that
are used by the build directly.

A reference for configuration values can be found in [src/config/config-types.d.ts](src/config/config-types.d.ts).

## Develop

This project uses

- [yarn 3](https://yarnpkg.com) as package manager
- [vite](https://vitejs.dev) as build-system
- [react](https://reactjs.org/) as UI framework
- [bootstrap](https://getbootstrap.com/) and [react-bootstrap](https://react-bootstrap.github.io/) as component library
- [vitest](https://vitest.dev/) and [react-testing-library](https://testing-library.com/docs/react-testing-library/intro/) for unit/integration tests

### Install dependencies

```
corepack enable
```

```
yarn
```

### Developmenet mode

The following command runs

- the dev-server
- the unit-tests in watch-mode
- the TypeScript compiler in watch-mode

```
yarn dev
```

Dev-Server with Mock-Backend (using msw.js)

```
VITE_USE_MOCK_API=true yarn dev
```

### Rebuild openapi-client from backend in Quarkus development mode (http://localhost:8080/)

(backend must run in `quarkus dev`)

```
yarn generate:openapi
```

### Run tests, typechecks, lint, check format (like in ci-pipeline)

```
yarn test
```

### Run code-formatter

```
yarn format
```

(the code formatter and linter are also run on pre-commit using (husky and lint-staged))
