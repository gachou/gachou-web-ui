/* global Promise */
// This is a dummy file that is copied to the docker image
// It is never used
// When using docker, the config is intercepted by a "respond" directive in Caddyfile
Promise.reject(`This file should not be deliverd to requesting browsers. 
There should be a 'respond' directive in the Caddyfile that intercepts requests to this file 
and delivers the configuration from the $GACHOU_FRONTEND_CONFIG environment variable`);
