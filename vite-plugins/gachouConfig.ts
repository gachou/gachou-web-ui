import { Plugin } from "vite";
import fs from "fs";
import path from "path";

const configDir = path.resolve(__dirname, "..", "config");

export function gachouConfig(configName = "default-config"): Plugin {
  const pathToConfigFile = resolveGachouConfigFile(configName);

  return {
    name: "load-gachou-config",
    config() {
      return {
        resolve: {
          alias: {
            "gachou-config.js": pathToConfigFile,
          },
        },
        build: {
          rollupOptions: {
            output: [
              {
                manualChunks(chunkId) {
                  if (chunkId === pathToConfigFile) {
                    return "gachou-config";
                  }
                },
                chunkFileNames(chunk) {
                  return chunk.name === "gachou-config"
                    ? "[name].js"
                    : "assets/[name].[hash].js";
                },
              },
            ],
          },
        },
      };
    },
  };
}

function resolveGachouConfigFile(configName: string): string {
  const configFile = path.resolve(configDir, `${configName}.js`);

  if (fs.existsSync(configFile)) {
    console.log("Using config file: " + configFile);
    return configFile;
  }

  const existingNames = findExistingConfigNames().join(", ");
  throw new Error(
    `Config "${configName}" not found, possible values: ${existingNames}`
  );
}

function findExistingConfigNames(): string[] {
  return fs
    .readdirSync(configDir)
    .filter((name) => name.endsWith(".js"))
    .map((config) => config.replace(/\.js$/, ""));
}
