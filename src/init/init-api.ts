import { ApiBaseUrl } from "../config/config-types";
import { defaults } from "../api/__generated__/openapi-client";

export async function initApi(baseUrl: ApiBaseUrl) {
  if (baseUrl === "mock-api://") {
    const mockApi = await import("src/development/mock-api");
    await mockApi.initMockApi();
  } else {
    defaults.baseUrl = baseUrl;
  }
}
