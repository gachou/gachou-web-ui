import { RecoilRoot as OriginalRecoilRoot, RecoilRootProps } from "recoil";
import React, { PropsWithChildren } from "react";

// In the types for React 18, the "PropsWithChildren" has been removed
// from the "FunctionComponent" definition. This causes compile errors
// when "<RecoilRoot>" has a child element (which is needs to have).
// see https://github.com/DefinitelyTyped/DefinitelyTyped/pull/56210/files#r846261799
//
// The following definition reverts that change.
export const RecoilRoot: React.FC<PropsWithChildren<RecoilRootProps>> =
  OriginalRecoilRoot;
