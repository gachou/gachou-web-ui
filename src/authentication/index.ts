import * as jwtUserPassword from "./jwtUserPassword";
import React from "react";

export interface User {
  username: string;
}

export interface UseAuthResult {
  authenticationInitialized: boolean;
  logout(): void;
  currentUser: User | null;
}

export type UseAuth = () => UseAuthResult;

const selectedMethod = jwtUserPassword;

export const useAuthentication: UseAuth = selectedMethod.useAuthentication;
export const LoginPage: React.FC = selectedMethod.LoginPage;
