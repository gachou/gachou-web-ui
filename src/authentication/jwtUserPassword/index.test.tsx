import { describe, expect, it } from "vitest";
import { act, fireEvent, screen, waitFor } from "@testing-library/react";
import { LoginPage } from "./LoginPage";
import { oas } from "src/api/mock-api-helpers";
import userEvent from "@testing-library/user-event";
import { customRender } from "src/test-utils/custom-render";
import { useAuthentication } from "./index";
import React from "react";
import { setupMockApi } from "src/test-utils/setup-mock-api";
import { fetchUserInfo } from "src/api/userInfo";

const BOBS_PASSWORD = "pw-bob";

const { requests } = setupMockApi(
  oas.post("/api/login", (req, res, context) => {
    if (req.body.password === BOBS_PASSWORD) {
      return res(
        context.json({
          token: "test",
        })
      );
    }
    return res(context.status(401));
  }),
  oas.get("/api/users/me", (req, res, context) => {
    return res(
      context.json({
        username: "test",
      })
    );
  })
);

async function waitForLogout() {
  await waitFor(() => {
    expect(screen.getByTestId("currentUser")).toBeEmptyDOMElement();
  });
}

async function clickLogoutButton() {
  await act(() => screen.getByText("Logout").click());
}

describe("authentication", () => {
  it("sends access token for requests after successful login", async () => {
    customRender(<LoginPageTestWrapper />);
    await submitLoginForm("bob", BOBS_PASSWORD);
    await waitForLoginSuccess();
    const usersMe = await captureUserInfoRequest();

    expect(usersMe.headers).toEqual(
      expect.objectContaining({ authorization: "Bearer test" })
    );
  });

  it("sends no access token if login failed", async () => {
    customRender(<LoginPageTestWrapper />);
    await submitLoginForm("bob", "wrong-password");
    await waitForFailedLogin();

    const usersMe = await captureUserInfoRequest();
    expect(usersMe.headers).toEqual(
      expect.not.objectContaining({ authorization: "Bearer test" })
    );
  });

  it("sends no access token after logout", async () => {
    customRender(<LoginPageTestWrapper />);
    await submitLoginForm("bob", BOBS_PASSWORD);
    await waitForLoginSuccess();
    await clickLogoutButton();
    await waitForLogout();
    const usersMe = await captureUserInfoRequest();
    expect(usersMe.headers).toEqual(
      expect.not.objectContaining({ authorization: "Bearer test" })
    );
  });

  it("keeps authentication across page reloads", async () => {
    const { unmount } = customRender(<LoginPageTestWrapper />);

    await submitLoginForm("bob", BOBS_PASSWORD);
    await waitForLoginSuccess();

    unmount();
    customRender(<LoginPageTestWrapper />);
    await waitForAuthInitialized();

    const usersMe = await captureUserInfoRequest();
    expect(usersMe.headers).toEqual(
      expect.objectContaining({ authorization: "Bearer test" })
    );
  });

  it("keeps logged-out state across page reloads", async () => {
    const { unmount } = customRender(<LoginPageTestWrapper />);

    await submitLoginForm("bob", BOBS_PASSWORD);
    await waitForLoginSuccess();
    await clickLogoutButton();
    await waitForLogout();

    unmount();
    customRender(<LoginPageTestWrapper />);
    await waitForAuthInitialized();

    const usersMe = await captureUserInfoRequest();
    expect(usersMe.headers).toEqual(
      expect.not.objectContaining({ authorization: "Bearer test" })
    );
  });
});

const LoginPageTestWrapper: React.FC = () => {
  const { currentUser, logout, authenticationInitialized } =
    useAuthentication();
  return (
    <div>
      <LoginPage />
      <button onClick={logout}>Logout</button>
      <button onClick={fetchUserInfo}>Fetch user-info</button>
      <div data-testid={"currentUser"}>{currentUser?.username}</div>
      <div data-testid={"initialized"}>
        {authenticationInitialized ? "yes" : "no"}
      </div>
    </div>
  );
};

async function submitLoginForm(username: string, password: string) {
  const usernameInput = screen.getByPlaceholderText("username");
  await act(() => userEvent.type(usernameInput, username));

  const passwordInput = screen.getByPlaceholderText("password");
  await userEvent.type(passwordInput, password);

  await act(async () => {
    fireEvent.click(screen.getByText("Login"));
  });
}

async function waitForAuthInitialized() {
  await waitFor(() => {
    expect(screen.getByTestId("initialized")).toHaveTextContent("yes");
  });
}

async function waitForLoginSuccess() {
  await waitFor(() => {
    expect(screen.getByTestId("currentUser")).toHaveTextContent("bob");
  });
}

async function waitForFailedLogin() {
  await screen.findByText("Login failed", { exact: false });
}

async function captureUserInfoRequest() {
  await fetchUserInfo();
  return await requests.find({
    pathname: "/api/users/me",
    method: "GET",
  });
}
