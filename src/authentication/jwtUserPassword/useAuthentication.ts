import { User } from "../index";
import { useRecoilState } from "recoil";
import { useAuthState } from "./authenticationStateAtom";
import { useEffect } from "react";
import { defaults } from "../../api/__generated__/openapi-client";
import { doLogin } from "../../api/auth/login";
import { UnauthorizedError } from "../../api/errors";
import { failureCountStateAtom } from "./failureCountStateAtom";

interface UseJwtUserPasswordResult {
  authenticationInitialized: boolean;
  login(username: string, password: string): Promise<void>;
  logout(): Promise<void>;
  currentUser: User | null;
  failureCount: number;
}

export function useAuthentication(): UseJwtUserPasswordResult {
  const [authState, setAuthState] = useAuthState();
  const [failureCount, setFailureCount] = useRecoilState(failureCountStateAtom);

  useEffect(() => {
    if (authState?.authenticated) {
      defaults.headers = {
        Authorization: "Bearer " + authState.accessToken,
      };
    } else {
      defaults.headers = {};
    }
  }, [authState]);

  return {
    failureCount,
    authenticationInitialized: authState != null,
    async logout() {
      setAuthState({ authenticated: false });
    },
    async login(username, password) {
      try {
        const accessToken = await doLogin(username, password);
        setAuthState({ username, accessToken, authenticated: true });
        setFailureCount(0);
      } catch (error) {
        if (error instanceof UnauthorizedError) {
          setAuthState({ authenticated: false });
          setFailureCount((count) => count + 1);
        }
      }
    },
    currentUser: authState?.authenticated
      ? { username: authState.username }
      : null,
  };
}
