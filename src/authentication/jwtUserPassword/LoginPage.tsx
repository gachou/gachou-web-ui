import React from "react";
import { Alert, Button, Col, FloatingLabel, Form, Row } from "react-bootstrap";
import { SubmitHandler, useForm } from "react-hook-form";
import { useAuthentication } from "./useAuthentication";
import { configuration } from "src/config";

interface Inputs {
  username: string;
  password: string;
}

export const LoginPage: React.FC = () => {
  const {
    register,
    handleSubmit,
    formState: {
      // TODO: proper feedback when login fails
      // eslint-disable-next-line @typescript-eslint/no-unused-vars
      errors,
    },
  } = useForm<Inputs>();

  const { login, failureCount } = useAuthentication();
  const onSubmit: SubmitHandler<Inputs> = async ({ username, password }) => {
    await login(username, password);
  };

  return (
    <Row className="h-100 align-items-center">
      <Col className={"visible-md"} />
      <Col xs={12} md={9} lg={6}>
        <p className="fw-bold fs-1">Please sign in</p>
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Floating>
            <FloatingLabel
              className={"mb-3"}
              label="Username"
              controlId={"loginPage--username"}
            >
              <Form.Control
                type="text"
                {...register("username")}
                placeholder="username"
              />
            </FloatingLabel>
            <FloatingLabel
              label="Password"
              className="mb-3"
              controlId={"loginPage--password"}
            >
              <Form.Control
                size="lg"
                type="password"
                {...register("password")}
                placeholder="password"
              />
            </FloatingLabel>
            {failureCount > 0 && (
              <Alert className={"mt-1"} variant={"danger"}>
                {failureCount > 1
                  ? failureCount + " login failures"
                  : "Login failed"}
              </Alert>
            )}
            <Button variant="primary" type="submit">
              Login
            </Button>
            {configuration.apiBaseUrl === "mock-api://" && (
              <Alert className={"mt-3"} variant={"info"}>
                Mocking backend via{" "}
                <a href="https://mswjs.io">Mock Service Worker</a>. Please use
                one of the following credentials:
                <ul className={"mt-3"}>
                  <li>
                    username: <b>bob</b>, password: <b>bob</b>
                  </li>
                  <li>
                    username: <b>alice</b>, password: <b>alice</b>
                  </li>
                </ul>
              </Alert>
            )}
          </Form.Floating>
        </Form>
      </Col>
      <Col />
    </Row>
  );
};
