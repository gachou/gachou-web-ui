type LocalStorageAdapter<T> = {
  get: () => T | null;
  set: (newValue: T | null) => void;
};

export function localStorageAdapter<T>(key: string): LocalStorageAdapter<T> {
  return {
    get: () => parseIfNotNull(localStorage.getItem(key)),
    set: (value) => {
      if (value == null) {
        localStorage.removeItem(key);
      } else {
        localStorage.setItem(key, JSON.stringify(value));
      }
    },
  };
}

function parseIfNotNull(jsonString: string | null) {
  if (jsonString == null) {
    return null;
  }
  return JSON.parse(jsonString);
}
