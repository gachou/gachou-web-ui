import { atom, useRecoilState } from "recoil";
import { localStorageAdapter } from "./localStorageAdapter";

export interface LoggedInState {
  authenticated: true;
  username: string;
  accessToken: string;
}

export interface LoggedOutState {
  authenticated: false;
}

export type AuthenticationState = LoggedInState | LoggedOutState;

const authStorage = localStorageAdapter<AuthenticationState>("gachou-auth");

const STATE_KEY = "gachou-auth";

const authenticationStateAtom = atom<AuthenticationState | null>({
  key: STATE_KEY,
  effects: [
    ({ setSelf }) => {
      setSelf(authStorage.get() || { authenticated: false });
    },
  ],
});

export function useAuthState(): [
  AuthenticationState | null,
  (newState: AuthenticationState) => void
] {
  const [authState, setAuthState] = useRecoilState(authenticationStateAtom);

  return [
    authState,
    (newState) => {
      authStorage.set(newState);
      setAuthState(newState);
    },
  ];
}
