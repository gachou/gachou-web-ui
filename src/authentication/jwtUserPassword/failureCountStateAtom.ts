import { atom } from "recoil";

export const failureCountStateAtom = atom<number>({
  key: "authentication-failures-" + Math.floor(Math.random() * 100000),
  default: 0,
});
