/* eslint-disable */
/**
 * Gachou Backend
 * 0.0.1
 * DO NOT MODIFY - This file has been generated using oazapfts.
 * See https://www.npmjs.com/package/oazapfts
 */
import * as Oazapfts from "oazapfts/lib/runtime";
import * as QS from "oazapfts/lib/runtime/query";
export const defaults: Oazapfts.RequestOpts = {
  baseUrl: "/",
};
const oazapfts = Oazapfts.runtime(defaults);
export const servers = {};
export type LoginRequest = {
  username: string;
  password: string;
};
export type JwtResponse = {
  token: string;
};
export type UserResponse = {
  username?: string;
};
export type TestUserDto = {
  username: string;
  password: string;
};
export type TestSetupRequest = {
  testUsers: TestUserDto[];
};
/**
 * Example for restricted resource
 */
export function adminExample(opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchText("/api/admin", {
    ...opts,
  });
}
/**
 * Returns a JWT for username and password
 */
export function login(
  loginRequest?: LoginRequest,
  opts?: Oazapfts.RequestOpts
) {
  return oazapfts.fetchJson<
    | {
        status: 200;
        data: JwtResponse;
      }
    | {
        status: 401;
      }
  >(
    "/api/login",
    oazapfts.json({
      ...opts,
      method: "POST",
      body: loginRequest,
    })
  );
}
/**
 * Returns data about the currently logged in user
 */
export function usersMe(opts?: Oazapfts.RequestOpts) {
  return oazapfts.fetchJson<{
    status: 200;
    data: UserResponse;
  }>("/api/users/me", {
    ...opts,
  });
}
/**
 * Setup test data for integration tests
 */
export function setupTestData(
  testSetupRequest?: TestSetupRequest,
  {
    xGachouTestSetupToken,
  }: {
    xGachouTestSetupToken?: string;
  } = {},
  opts?: Oazapfts.RequestOpts
) {
  return oazapfts.fetchText(
    "/devtest/reset-gachou-for-tests",
    oazapfts.json({
      ...opts,
      method: "POST",
      body: testSetupRequest,
      headers: {
        ...(opts && opts.headers),
        "X-Gachou-Test-Setup-Token": xGachouTestSetupToken,
      },
    })
  );
}
