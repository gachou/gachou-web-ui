We use the npm-package "oazapfts" to generate an api-client.

Alternatives:

- https://www.npmjs.com/package/openapi-typescript-codegen
- openapi-client-axios does not work well with vite
