export class HttpError extends Error {
  readonly status: number;
  readonly data?: unknown;

  constructor(status: number, data?: unknown) {
    super(`Error: ${status}`);
    this.status = status;
    this.data = data;
  }
}

export class UnauthorizedError extends HttpError {
  constructor() {
    super(401);
  }
}
