import { usersMe } from "./__generated__/openapi-client";

interface UserInfo {
  username?: string;
}

export async function fetchUserInfo(): Promise<UserInfo> {
  const response = await usersMe();
  return response.data;
}
