import * as api from "src/api/__generated__/openapi-client";
import { handle, HttpError } from "oazapfts";
import { UnauthorizedError } from "../errors";

export async function doLogin(
  username: string,
  password: string
): Promise<string> {
  return await handle(api.login({ username, password }), {
    200(response) {
      return response.token;
    },
    401() {
      throw new UnauthorizedError();
    },
    default(status, data) {
      throw new HttpError(status, data);
    },
  });
}

export function doLogout() {
  api.defaults.headers = {};
}
