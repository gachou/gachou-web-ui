import {
  MswGetOperations,
  MswPostOperations,
  paths,
} from "../__generated__/openapi-types";
import { rest, RestHandler } from "msw";

type OpenApiPaths = keyof paths;

export type RestHandlerType<Resolvers> = <P extends keyof Resolvers>(
  path: P,
  resolver: Resolvers[P]
) => RestHandler;

export const oas = {
  post: rest.post.bind(rest) as RestHandlerType<MswPostOperations>,
  get: rest.get.bind(rest) as RestHandlerType<MswGetOperations>,
} as const;

export function path<T extends OpenApiPaths>(path: T) {
  return path;
}
