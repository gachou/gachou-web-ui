import React from "react";
import ReactDOM from "react-dom";

import "./styles/index.css";
import { RecoilRoot } from "./types/recoilroot-workaround";
import App from "./App";
import { initApi } from "./init/init-api";
import { configuration } from "./config";
import { RecoilDebugObserver } from "./utils/RecoilDebugObserver";

initApi(configuration.apiBaseUrl).then(() => {
  ReactDOM.render(
    <React.StrictMode>
      <RecoilRoot>
        {process.env.NODE_ENV === "development" && <RecoilDebugObserver />}
        <App />
      </RecoilRoot>
    </React.StrictMode>,
    document.getElementById("root")
  );
});
