import React from "react";

import logo from "src/assets/images/favicon.svg";

export const GachouLogo: React.FC<{
  size: string;
  scale?: number;
  className?: string;
}> = ({ size, scale = 1, className }) => {
  return (
    <img
      alt="Logo"
      src={logo}
      className={className}
      style={{
        width: size,
        height: size,
        transform: `scale(${scale})`,
        transformOrigin: "50% 50%",
      }}
    />
  );
};
