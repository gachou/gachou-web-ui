import React from "react";
import { Container, Nav, Navbar, NavDropdown } from "react-bootstrap";
import { GachouLogo } from "src/components/atoms/GachouLogo";
import { useAuthentication } from "../../../authentication";

export const GachouNavbar: React.FC<{ className?: string }> = ({
  className,
}) => {
  const { currentUser, logout } = useAuthentication();

  return (
    <Navbar bg="light" expand="lg" className={className}>
      <Container>
        <Navbar.Brand href="#home">
          <GachouLogo size={"2rem"} className={"me-2"} />
          <span className={"text-primary fw-bold"}>Gachou</span>
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="gachou-navbar__collapse" />
        {currentUser != null && (
          <Navbar.Collapse id="gachou-navbar__collapse">
            <Nav className="ms-auto">
              <NavDropdown
                title={"User: " + currentUser.username}
                data-testid={"user-menu"}
              >
                <NavDropdown.Item onClick={logout}>Logout</NavDropdown.Item>
              </NavDropdown>
            </Nav>
          </Navbar.Collapse>
        )}
      </Container>
    </Navbar>
  );
};
