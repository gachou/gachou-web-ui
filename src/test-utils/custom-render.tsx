import { render, RenderOptions, RenderResult } from "@testing-library/react";
import React from "react";
import { RecoilRoot } from "../types/recoilroot-workaround";

export function customRender(
  ui: React.ReactElement,
  options?: Omit<RenderOptions, "queries">
): RenderResult {
  return render(<RecoilRoot>{ui}</RecoilRoot>, options);
}
