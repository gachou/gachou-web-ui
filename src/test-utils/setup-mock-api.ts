import { DefaultRequestBody, RequestHandler } from "msw";
import { setupServer } from "msw/node";
import { afterEach, beforeEach, expect } from "vitest";
import { waitFor } from "@testing-library/react";

interface SetupMockApiResult {
  overrideHandlers(...requestHandlers: RequestHandler[]): void;
  expectRequestToHaveBeenSent(request: Partial<CapturedRequest>): void;
  requests: Queries<CapturedRequest>;
}

interface CapturedRequest {
  method: string;
  pathname: string;
  headers: Record<string, string>;
  body?: DefaultRequestBody;
}

export function setupMockApi(
  ...requestHandlers: RequestHandler[]
): SetupMockApiResult {
  const server = setupServer(...requestHandlers);
  let capturedRequests: CapturedRequest[] = [];

  beforeEach(async () => {
    capturedRequests = [];
    server.listen({ onUnhandledRequest: "error" });
    server.events.on("request:end", (request) => {
      capturedRequests.push({
        method: request.method,
        pathname: request.url.pathname,
        headers: request.headers.all(),
        body: request.body,
      });
    });
  });

  afterEach(async () => {
    server.resetHandlers();
    server.close();
  });

  function queryAllCapturedRequests(
    partialRequest: Partial<CapturedRequest>
  ): CapturedRequest[] {
    const matcher = expect.objectContaining(partialRequest);
    return capturedRequests.filter((request) =>
      matcher.asymmetricMatch(request)
    );
  }

  return {
    overrideHandlers(...requestHandlers) {
      server.use(...requestHandlers);
    },
    expectRequestToHaveBeenSent(request: Partial<CapturedRequest>) {
      expect(capturedRequests).toContainEqual(expect.objectContaining(request));
    },
    requests: buildQueries(
      queryAllCapturedRequests,
      "No request found",
      "Multiple requests found"
    ),
  };
}

// see https://testing-library.com/docs/queries/about for semantics
interface Queries<T> {
  queryAll(needle: Partial<T>): T[];
  getAll(needle: Partial<T>): T[];
  findAll(needle: Partial<T>): Promise<T[]>;
  query(needle: Partial<T>): T | null;
  get(needle: Partial<T>): T;
  find(needle: Partial<T>): Promise<T>;
}

function buildQueries<T>(
  queryAll: Queries<T>["queryAll"],
  resultEmptyError: string,
  multipleResultsError: string
): Queries<T> {
  function assertNotEmpty(result: T[]): void {
    if (result.length === 0) {
      throw new Error(resultEmptyError);
    }
  }

  function assertSingle(result: T[]): void {
    if (result.length > 1) {
      throw new Error(multipleResultsError);
    }
  }

  const getAll: Queries<T>["getAll"] = (needle) => {
    const result = queryAll(needle);
    assertNotEmpty(result);
    return result;
  };

  const get: Queries<T>["get"] = (needle) => {
    const result = queryAll(needle);
    assertNotEmpty(result);
    assertSingle(result);
    return result[0];
  };

  return {
    queryAll,
    query(needle) {
      const result = queryAll(needle);
      assertSingle(result);
      if (result.length === 0) {
        return null;
      }
      return result[0];
    },
    getAll,
    get,
    async findAll(needle) {
      return waitFor(() => {
        return getAll(needle);
      });
    },
    async find(needle) {
      return waitFor(() => {
        return get(needle);
      });
    },
  };
}
