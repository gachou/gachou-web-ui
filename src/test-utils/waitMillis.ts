export function delayMillis(millis: number) {
  return new Promise((resolve) => setTimeout(resolve, millis));
}
