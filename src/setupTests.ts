/* eslint-disable @typescript-eslint/no-explicit-any */

import { afterEach, beforeAll, expect } from "vitest";
import "whatwg-fetch";
import matchers from "@testing-library/jest-dom/matchers";
import { cleanup } from "@testing-library/react";

workaroundForReactActEnvironment();

beforeAll(() => {
  expect.extend(matchers);
});

afterEach(cleanup);

function workaroundForReactActEnvironment() {
  // synchronize value of IS_REACT_ACT_ENVIRONMENT from globalThis to globalThis.self,
  // to ensure that waitForProxies of "@testing-library/react" set the value on the
  // correct global object.
  proxyProperty(globalThis, globalThis.self, "IS_REACT_ACT_ENVIRONMENT");
  // fix warning: "Warning: The current testing environment is not configured to support act(...)"
  (globalThis as any).IS_REACT_ACT_ENVIRONMENT = true;
}

function proxyProperty(source: any, target: any, propertyName: string) {
  Object.defineProperty(globalThis, propertyName, {
    get() {
      return target[propertyName];
    },
    set(value: any) {
      target[propertyName] = value;
    },
  });
}

export {};
