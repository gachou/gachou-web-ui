import { expect, test } from "vitest";
import { screen } from "@testing-library/react";
import App from "./App";
import "react";
import { customRender } from "./test-utils/custom-render";

test("should contain the logo", () => {
  customRender(<App />);

  expect(screen.queryByAltText("Logo")).not.toBeNull();
});

// TODO: Check if more tests are appropriate
