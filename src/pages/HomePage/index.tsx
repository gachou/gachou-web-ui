import React, { useEffect, useState } from "react";
import { fetchUserInfo } from "src/api/userInfo";

const Homepage: React.FC = () => {
  const [me, setMe] = useState<string>();

  useEffect(() => {
    fetchUserInfo().then((result) => setMe(result.username));
  }, []);
  return (
    <div>
      <h1>Homepage</h1>
      <p>I am {me}</p>
      <p>
        There is not a lot to see yet, since I am still in very early stages of
        development. The most interesting part may yet be the{" "}
        <a href={"https://gitlab.com/gachou/gachou-web-ui"}>project setup</a>
      </p>
    </div>
  );
};

export default Homepage;
