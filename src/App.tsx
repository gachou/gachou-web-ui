import { Container } from "react-bootstrap";
import React, { lazy } from "react";
import { GachouRouter } from "./routes/GachouRouter";
import { LoginPage, useAuthentication } from "./authentication";
import { GachouNavbar } from "./components/molecules/GachouNavbar";

const App: React.FC = () => {
  const { currentUser } = useAuthentication();

  return (
    <div>
      <GachouNavbar className={"mb-3"} />
      <Container>
        {currentUser == null ? <LoginPage /> : <RouterView />}
      </Container>
    </div>
  );
};

const RouterView: React.FC = () => {
  return (
    <GachouRouter
      routeComponents={{
        "/": lazy(() => import("src/pages/HomePage")),
      }}
      notFound={lazy(() => import("src/pages/NotFound"))}
    />
  );
};

export default App;
