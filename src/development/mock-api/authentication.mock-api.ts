import { oas } from "src/api/mock-api-helpers";

export function createAuthenticationHandlers() {
  const users: Record<string, { password: string }> = {
    bob: { password: "bob" },
    alice: { password: "alice" },
  };

  return [
    oas.post("/api/login", (req, res, context) => {
      const username = req.body.username;
      const user = users[username];
      if (user == null) {
        return res(context.status(401));
      }
      if (user.password !== req.body.password) {
        return res(context.status(401));
      }
      return res(context.json({ token: btoa(JSON.stringify({ username })) }));
    }),
  ];
}
