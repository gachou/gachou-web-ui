import { setupWorker } from "msw";
import { createAuthenticationHandlers } from "./authentication.mock-api";

export async function initMockApi(): Promise<void> {
  const worker = setupWorker(...createAuthenticationHandlers());
  await worker.start();
}
