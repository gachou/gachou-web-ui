import "gachou-config.js";
import { GachouConfiguration } from "./config-types";

export const configuration: GachouConfiguration = {
  apiBaseUrl: "http://localhost",
  ...window.GACHOU_WEB_UI_CONFIG,
};
