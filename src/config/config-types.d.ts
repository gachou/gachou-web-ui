export declare global {
  interface Window {
    GACHOU_WEB_UI_CONFIG: Partial<GachouConfiguration>;
  }
}

export type ApiBaseUrl = string | "mock-api://";

export interface GachouConfiguration {
  /**
   * The root URL of the Gachou backend service.
   * It either starts with `http://`, `https://` or equals `mock-api://`
   *
   * When `mock-api://` is specified, https://mswjs.io/ to simulate a
   * backend using the service-worker. This only works if the frontend is
   * served from `localhost` or via `https`.
   */
  apiBaseUrl: ApiBaseUrl;
}
