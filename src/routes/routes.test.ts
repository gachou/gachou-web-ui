import { resolveRoute } from "./routes";
import { describe, expect, it } from "vitest";

describe("resolveRoute", () => {
  it("returns the parameter as value", () => {
    expect(resolveRoute("/")).toBe("/");
  });
});
