import { RoutePattern } from "./routes";
import React, { Suspense } from "react";
import { Route, RouteProps, Router } from "wouter";

interface GachouRouterProps {
  routeComponents: RouteComponents;
  notFound: React.ComponentType;
}

type RouteComponents = {
  [P in RoutePattern]: RouteProps<undefined, P>["component"];
};

export const GachouRouter: React.FC<GachouRouterProps> = ({
  routeComponents,
  // TODO: add not-found route
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  notFound,
}) => {
  return (
    <Suspense>
      <Router>
        {Object.entries(routeComponents).map(([path, pageComponent]) => {
          return <Route key={path} path={path} component={pageComponent} />;
        })}
      </Router>
    </Suspense>
  );
};
