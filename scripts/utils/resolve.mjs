import path from "path";

import { packageDirectorySync } from "pkg-dir";

const packageDir = packageDirectorySync();

export function resolve(posixPath) {
  const pathComponents = posixPath.split("/");
  return path.resolve(packageDir, ...pathComponents);
}
