// https://github.com/cosee/techtalk-typescript-2021-02/blob/master/scripts/utils/prepare-spec.js
import fs from "fs";
import prettier from "prettier";
import openapiTypeScript from "openapi-typescript";

export async function writeSpec(specFile, spec) {
  const jsonData = JSON.stringify(spec, removeExtensionProps);
  const prettySpec = prettier.format(jsonData, { parser: "json" });
  fs.writeFileSync(specFile, prettySpec);
}

const removeExtensionProps = (key, value) =>
  key.match(/^x-/) ? undefined : value;

export async function writeTypeScriptSpec(typescriptSpecFile, spec) {
  const typeScriptData = `
  /* eslint-disable */
  import { RestRequest, ResponseResolver, rest, RestContext } from "msw";
  
  // Generated by "openapi-typescript"
  ${await openapiTypeScript(spec, { immutableTypes: true })};
  
  // Mock service worker types
  ${await generateTypesForMockServiceWorker(spec)};
  `;
  const prettySpec = prettier.format(typeScriptData, { parser: "typescript" });
  fs.writeFileSync(typescriptSpecFile, prettySpec);
}

export async function generateTypesForMockServiceWorker(spec) {
  const methodPathOperations = {
    get: {},
    post: {},
    patch: {},
    put: {},
    delete: {},
  };
  for (const [path, operations] of Object.entries(spec.paths)) {
    for (const [method, operation] of Object.entries(operations)) {
      methodPathOperations[method][path] = operation;
    }
  }
  return ` 
     ${createMswOperations("MswPostOperations", methodPathOperations.post)};
     ${createMswOperations("MswGetOperations", methodPathOperations.get)};
  `;
}

function createMswOperations(interfaceName, operationsByPath) {
  if (Object.keys(operationsByPath).length === 0) {
    return "";
  }
  return `export interface ${interfaceName} {
      ${Object.entries(operationsByPath).map(
        ([path, operation]) =>
          `${JSON.stringify(path)}: ${buildResponseResolver(path, operation)}`
      )}
  }`;
}

function buildResponseResolver(path, operation) {
  return `ResponseResolver<
            ${buildRequestType(operation)},
            RestContext,
            ${buildResponseType(operation)}
          >`;
}

function buildRequestType(operation) {
  const escapedOperationId = JSON.stringify(operation.operationId);
  return hasDefinedRequestBody(operation)
    ? `RestRequest<operations[${escapedOperationId}]["requestBody"]["content"]["application/json"]>`
    : "RestRequest<never>";
}

function hasDefinedRequestBody(operation) {
  return operation.requestBody?.content["application/json"] != null;
}

function buildResponseType(operation) {
  const escapedOperationId = JSON.stringify(operation.operationId);
  const validStatusCodes = Object.keys(operation.responses).filter((status) =>
    hasDefinedResponseBody(operation, status)
  );
  const statusCodeUnion = validStatusCodes.join("|");
  return statusCodeUnion !== ""
    ? `operations[${escapedOperationId}]["responses"][${statusCodeUnion}]["content"]["application/json"]`
    : "never";
}

function hasDefinedResponseBody(operation, responseCode) {
  return (
    operation.responses[responseCode].content?.["application/json"] != null
  );
}
