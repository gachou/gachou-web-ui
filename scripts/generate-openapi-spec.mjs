/* eslint-env node */

import axios from "axios";
import fs from "fs";
import cp from "child_process";
import { writeSpec, writeTypeScriptSpec } from "./utils/prepare-spec.mjs";
import prettier from "prettier";
import { resolve } from "./utils/resolve.mjs";

const specFile = resolve("src/api/__generated__/openapi.json");
const typeScriptSpecFile = resolve("src/api/__generated__/openapi-types.ts");
const openapiClientFile = resolve("src/api/__generated__/openapi-client.ts");

async function buildFromOpenAPI() {
  const spec = await downloadSpec();
  await writeSpec(specFile, spec);
  await writeTypeScriptSpec(typeScriptSpecFile, spec);
  await writeOpenapiClient(specFile, openapiClientFile);
}

async function downloadSpec() {
  const response = await axios.get("http://localhost:8080/q/openapi");
  return response.data;
}

async function writeOpenapiClient(specFile, typesFile) {
  const result = cp.execFileSync("oazapfts", [specFile]);
  const dtsContents = "/* eslint-disable */\n" + result;
  const formattedDts = prettier.format(dtsContents, { parser: "typescript" });
  fs.writeFileSync(typesFile, formattedDts);
}

buildFromOpenAPI().catch(console.error);
