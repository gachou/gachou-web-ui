/// <reference types="vitest" />
/// <reference types="vite/client" />

import { defineConfig } from "vite";
import react from "@vitejs/plugin-react";
import path from "path";
import { copyMockServiceWorker } from "./vite-plugins/copyMockServiceWorker";
import { gachouConfig } from "./vite-plugins/gachouConfig";

const outDir = path.resolve(__dirname, "dist");

export default defineConfig({
  plugins: [
    react(),
    gachouConfig(process.env.GACHOU_WEB_UI_CONFIG),
    copyMockServiceWorker(),
  ],
  resolve: {
    alias: {
      src: path.resolve(__dirname, "src"),
    },
  },
  root: "src",
  build: {
    outDir,
  },
  test: {
    globals: true,
    environment: "jsdom",
    setupFiles: ["./src/setupTests.ts"],
  },
});
